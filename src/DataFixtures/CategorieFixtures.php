<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Faker;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {   
        // Instancie Faker
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for () pour choisir le nombre d'élements allant en BBD
        for($i = 0; $i <= 10; $i++){
            $category = new Categorie();
            $category->setName($faker->colorName);
            $category->setSlug((new AsciiSlugger())->slug(strtolower($faker->colorName)));

            // Enregistre l'objet dans une référence. cad que l'on pourra utiliser les objets enregistrés dans une autre fixture afin de pouvoir effectuer des relations de table.
            // Le premier paramètre est un nom que se doit d'etre UNIQUE
            // Le second paramètre est l'objet qui sera lié à se nom
            $this->addReference('category_'.$i, $category);

            // Garde de côté en attendant l'éxécution des requêtes
            $manager->persist($category);
        }

        $manager->flush();
    }
}
