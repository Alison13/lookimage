<?php

namespace App\DataFixtures;

use App\Entity\Picture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Mmo\Faker\PicsumProvider;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class PictureFixtures extends Fixture implements DependentFixtureInterface
{  
    // permet de dire à notre fixture si elle depend d'autre fixture afin de donner un ordre a notre insertion de donnée et de ne pas avoir d'erreurs lors de l'utilisation de "getReference()"

    public function getDependencies()
    {   // ici l'insertion dans picture se fera en dernier
        return[
            CategorieFixtures::class,
            UserFixtures::class
        ];
    }
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        // On ajoute le bundle "Mmo Faker-image" à Faker
        $faker->addProvider(new PicsumProvider($faker));

        for($i = 0; $i <= 50; $i++){

            // Spécifier le chemin du dossier d'upload
            // Les deux paramètres sont la hauteur et la largeur de l'image à récupérer
            $image = $faker->picsum('./public/uploads/images/featured', random_int(1152,2312), random_int(864,1736));
            // Récupération d'une référence aléatoirement.
            // On récupère un objet de l'entité Catégorie généré dans le fichier CategorieFixtures grâce au nom choisi lors de l'enregistrement dans les référence
            $category = $this->getReference('category_'. random_int(0,10));
            // On récupère une référence utilisateur aléatoirement
            $user = $this->getReference('user_'. random_int(0,10));

            $picture = new Picture();
            $picture->setDescription($faker->sentence(26));
            $picture->setTags($faker->emoji);
            $picture->setCreatedAt($faker->dateTimeBetween('-4 years'));
            $picture->setUpdatedAt($faker->dateTimeBetween('-3 years'));
            $picture->setCategory($category);
            $picture->setUser($user);

            //Gestion de l'image
            // use Symfony\Component\HttpFoundation\File\File;
            $picture->setImageFile(new File($image));
            $picture->setImage(str_replace('./public/uploads/images/featured/', '', $image));

            // Garde de côté en attendant l'éxécution des requêtes
            $manager->persist($picture);
        }

        $manager->flush();
    }
}

