<?php

namespace App\Form;

use App\Entity\Categorie;
use App\Entity\Picture;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\NotBlank;

class UploadPictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('imageFile', VichImageType::class,[
                'required' => false,
                'label' => 'Selectionner une photo',
                // Option de VichImageType
                'allow_delete'=> false,
                'download_uri' => false,
                'imagine_pattern' => 'mini2',
                'attr' => ['class'=> 'image'],
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir une photo.',
                        'groups' => ['new_picture']
                    ]),
                    new Image([
                        'maxSize' => '2M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 2Mo.',
                        'mimeTypes' => [
                            'image/gif', 'image/png', 'image/jpeg', 'image/webp'
                        ],
                        'mimeTypesMessage' => 'Le format de l\'image est invalide.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('description', TextareaType::class,[
                'required' => true,
                'label' => 'Description de l\'image',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez saisir une description.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('tags', TextType::class,[
                'required' => true,
                'label' => 'Tag(s)',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir au moins un tag.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
            ->add('category', EntityType::class,[
                'required' => true,
                'label' => 'Catégorie',
                'class' => Categorie::class,
                'choice_label' => 'name',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir la catégorie.',
                        'groups' => ['new_picture', 'edit_picture']
                    ])
                ]
            ])
        
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
