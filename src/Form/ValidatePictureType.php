<?php

namespace App\Form;

use App\Entity\Picture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ValidatePictureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder->add('is_validated', ChoiceType::class, [
            'choices' => [
                'valide' => true,
                'invalide' => false
            ]
        ])
             ->add('imageFile', VichImageType::class,[
                'required' => false,
             
                // Option de VichImageType
                'allow_delete'=> false,
                'download_uri' => false,
                'imagine_pattern' => 'thumbnail',
                'attr' => ['class'=> 'image'],
                
                 
            ]);
        
    }
   
    
   
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Picture::class,
        ]);
    }
}
