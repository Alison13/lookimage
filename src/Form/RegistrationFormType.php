<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
// use Symfony\Component\Validator\Constraints\NotEqualTo;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'required' => true, // "Champs obligatoire". Ce n'est pas obligatoire mais on le met pour se souvenir car il a déja été entré comme "not nul" lors de sa création dans le terminal. ne peut plus etre rendu facultatif(false).
                'label' => 'Prénom',// changer le nom label, false si on ne veut rien mettre
                'attr' => ['class'=> 'formulaire'// config pour ajouter une classe a notre input
                ],
                'constraints' => [ // Contraintes de validations. Vérification des données côté serveur. : on vérifie que le champ n'est pas vide et message d'erreur
                    new NotBlank(['message' => 'Veuillez saisir un prénom'])
                ]
            ])// TextType : Symfony\Component\Form\Extension\Core\Type\TextType;
            ->add('lastname', TextType::class,[
                'required' => true,
                'label' => 'Nom',
                'attr' => ['class'=> 'formulaire'],
                'constraints' => [new NotBlank(['message' => 'Veuillez saisir un nom'])]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse email',
                'attr' => ['class'=> 'formulaire'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir une adresse email.']),
                    new Email([
                        'message' => 'Veuillez saisir une adresse email valide.'])
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false, // signifie qu'il ne se trouve pas dans la base de données. ici 'agreeTerms'
                'label' => 'En soumettant ce formulaire, j\'accepte que MONSITE conserve mes données personnelles via ce formulaire. Aucune exploitation commerciale ne sera faite des données conservées.',
                'attr' => ['class'=> 'checkbox'],
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les RGPD.',
                    ])
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne sont pas identiques',
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
