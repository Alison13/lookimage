<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',  EmailType::class,[
                'required' => true,
                'label' => 'Prénom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir au moins un tag.',
                    
                    ])
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'User' => 'ROLE_USER',
                    'Editor' => 'ROLE_EDITOR',
                    'Admin' => 'ROLE_ADMIN'
                ],
                // 'attr' => ['class'=> 'checkbox'
                // ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles' 
            ])

            ->add('firstname', TextType::class,[
                'required' => true,
                'label' => 'Email',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir au moins un tag.',
                      
                    ])
                ]
            ])
            ->add('lastname', TextType::class,[
                'required' => true,
                'label' => 'Nom',
                'constraints' => [
                    new NotBlank([
                        'message'=> 'Veuillez choisir au moins un tag.',
                    
                    ])
                ]
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
