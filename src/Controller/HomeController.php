<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Picture;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request; // Nous avons besoin d'accéder à la requête pour obtenir le numéro de page
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        // équivalent : SELECT * FROM picture
        // findAll() retourne tous les résultats trouvés dans la table 'pictures'
        // $pictures = $this->getDoctrine()->getRepository(Picture::class)->findAll();
        $resultsPictures = $this->getDoctrine()->getRepository(Picture::class)->findBy(['is_validated' => true],['created_at' => 'desc']);
        $page =  $request->query->getInt('page', 1); // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
        $pictures = $paginator->paginate( 
        //paginate attend 3 résultats:
            $resultsPictures, // Requête contenant les données à paginer (ici nos images)
            $page === 0 ? 1 : $page, // verification de la valeur dans l'url
            10 // Nombre de résultats par page
        );

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('home/index.html.twig', [
            'pictures' => $pictures,
            'categories' => $categories,
          
            ]);
    }

    /** 
     * On donne un nom à la page, le name doit etre unique pour pouvoir l'utiliser dans les liens
     * @Route("/category/{id}", name="categorie")
     */
    public function categoriePage($id,Request $request, PaginatorInterface $paginator){

        $categoriePage = $this->getDoctrine()->getRepository(Categorie::class)->findOneBy(['id' => $id]);
        $query=$this->getDoctrine()->getRepository(Picture::class)->findBy([
            
            'category' => $categoriePage,
            'is_validated' => true
            
            ]);

        if(!$categoriePage){
            throw $this-> createNotFoundException('La catégorie n\'existe pas');
        }
        
        $page =  $request->query->getInt('page', 1);
        $pictures = $paginator->paginate( 
           $query,
            $page === 0 ? 1 : $page,
            10
        );

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        // retourne la vue associée à cette route (méthode)
        return $this->render('home/category.html.twig', [
                'categoriePage' => $categoriePage,
                'pictures' => $pictures,
                'categories' => $categories
       
        ]);
    }
    /** 
     * On donne un nom à la page, le name doit etre unique pour pouvoir l'utiliser dans les liens
     * le requirements sert à valider le type de la donnée que l'on va recevoir. ici id(int)
     * @Route("/categoryimg/{id}", name="showImage", requirements={"id"="\d+"})
     */
    public function showImage($id,Request $request, PaginatorInterface $paginator){

        $showImage = $this->getDoctrine()->getRepository(Picture::class)->findOneBy(['id' => $id]);
        $query = $this->getDoctrine()->getRepository(Picture::class)->findBy([
            'category' =>$showImage -> getCategory(),
            'is_validated' => true

        ]);


        if(!$showImage){
            throw $this-> createNotFoundException('L\'image n\'existe pas');
        }
        
        $page =  $request->query->getInt('page', 1);
        $photos = $paginator->paginate( 
            $query,
            $page === 0 ? 1 : $page,
            5
        );
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        // retourne la vue associée à cette route (méthode)
        return $this->render('home/categoryimg.html.twig', [
                'showImage' => $showImage,
                'photos' => $photos,
                'categories' => $categories
        ]);
    }
}