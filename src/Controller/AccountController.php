<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
use App\Entity\Picture;
use App\Entity\User;
use App\Form\UploadPictureType;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="account")
     */
    // public function index(): Response
    // {
    //     return $this->render('account/index.html.twig', [
    //         'controller_name' => 'AccountController',
    //     ]);
    // }
    /** 
     * @Route("/account", name="importImages")
     */
    public function importImages(Request $request, PaginatorInterface $paginator){

    // $importImages = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);
  
    // $page =  $request->query->getInt('page', 1);
    // $pictures = $paginator->paginate( 
    //     $importImages -> getPictures(),
    //     $page === 0 ? 1 : $page,
    //     20
    // );

    // methode pour afficher les image selon le compte utilisateur connecté :

    $page = $request->query->getInt('page', 1);
    $query = $this->getDoctrine()->getRepository(Picture::class)->findBy(['user' => $this->getUser(),'is_validated' => true]);
    $pictures = $paginator->paginate( 
        $query,
        // $this->getUser()->getPictures(),
        $page === 0 ? 1 : $page,
        20
    );
    // retourne la vue associée à cette route (méthode)
    return $this->render('account/index.html.twig', [
            // 'importImages' => $importImages,
            'pictures' => $pictures
    ]);
}

    /** 
     * @Route("/account/new/picture", name="account_new_picture")
     */
    public function new(Request $request){
        
        // Instanciation de l'entité "Picture"
        $picture = new Picture();
        // Premier paramètre : Le formulaire dont on a besoin
        // Deuxième paramètre : L'objet de l'entité à vide
        $formUpload = $this-> createForm(UploadPictureType::class, $picture, ['validation_groups' => 'new_picture']);
        $formUpload->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formUpload->isSubmitted() && $formUpload->isValid()){
            // Ajoute la date du jour
            $picture->setCreatedAt(new \DateTimeImmutable());
            // Passe l'utilisateur actuellement connecté à notre setter
            $picture->setUser($this->getUser());
            
            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success','Merci pour votre partage !');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('importImages');
        }

       // capte toutes les erreurs d'un formulaire pour les afficher où l'on veut.
        // if ($formUpload->isSubmitted() && !$formUpload->isValid()) {
        //     $errors = $formUpload->getErrors(true);
        // }

        return $this->render('account/new.html.twig', [
            'formUpload' => $formUpload->createView(),
            // 'errors' => $errors
    ]);
    }

     /** 
     * @Route("/account/edit/picture/{id}", name="account_edit_picture")
     */
    public function edit($id, Request $request){
        
        // Sélectionner un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // SI l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404
        if(!$picture || $this->getUser() !== $picture->getUser()){
            throw $this-> createNotFoundException('L\'image n\'existe pas');
        }

        $formEdit = $this-> createForm(UploadPictureType::class, $picture, ['validation_groups' => 'edit_picture']);
        $formEdit->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
            // Ajoute la date du jour
            $picture->setUpdatedAt(new \DateTimeImmutable());
            
            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success','Votre image a bien été modifié !');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('importImages');
        }

        return $this->render('account/edit.html.twig', [
            'formEdit' => $formEdit->createView(),
    
    ]);
    }

    /** 
     * @Route("/account/delete/picture/{id}", name="account_delete_picture")
     */
    public function delete($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        // SI l'image n'existe pas, on retourne une erreur 404
        // Ou si l'image n'appartient pas à l'utilisateur connecté, on retourne une erreur 404
        if(!$picture || $this->getUser() !== $picture->getUser()){
            throw $this-> createNotFoundException('L\'image n\'existe pas');// erreur 404 - Ressource non trouvée
            // throw $this->createAccessDeniedException('Cette image n\'existe pas'); // erreur 403 - accès refusé
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success','La photo à bien été supprimée');
        return $this->redirectToRoute('importImages');
    }

}