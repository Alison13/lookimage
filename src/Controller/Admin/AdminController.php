<?php

namespace App\Controller\Admin;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categorie;
use App\Entity\Picture;
use App\Form\AddCategoryType;
use App\Form\UploadPictureType;
use App\Form\ValidatePictureType;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin", name="admin")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $query = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
     

        $page =  $request->query->getInt('page', 1);
        $categories = $paginator->paginate( 
            $query, 
            $page === 0 ? 1 : $page, 
            5
        );

        return $this->render('admin/index.html.twig', [
            'categories' => $categories
    ]);
    }
    
     /**
     * @IsGranted("ROLE_EDITOR")
     * @Route("/admin/picture", name="admin_picture")
     */
    public function picture(Request $request, PaginatorInterface $paginator): Response
    {
       
        $query = $this->getDoctrine()->getRepository(Picture::class)->findAll();

        $page =  $request->query->getInt('page', 1);
        $pictures = $paginator->paginate( 
            $query, 
            $page === 0 ? 1 : $page, 
            15
        );

        return $this->render('admin/picture.html.twig', [
            'pictures' => $pictures
    ]);
    }

    /** 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/new/categorie", name="admin_new_categorie")
     */
    public function new(Request $request){
        
        $categorie = new Categorie();
       
        $formAdd = $this-> createForm(AddCategoryType::class, $categorie, ['validation_groups' => 'new_categorie']);
        $formAdd->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formAdd->isSubmitted() && $formAdd->isValid()){
            // transforme le nom de la catégorie en slug
            $categorie->setSlug((new AsciiSlugger())->slug(strtolower($categorie->getName())));

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($categorie);
            $doctrine->flush();

            $this->addFlash('success','la categorie a bien été ajouté !');
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/new.html.twig', [
            'formAdd' => $formAdd->createView(),
        
    ]);
    }

    /** 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/edit/categorie/{id}", name="admin_edit_categorie")
     */
    public function edit($id, Request $request){
        
        // Sélectionner un enregistrement en BDD
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        if (!$categorie){
            throw $this->createNotFoundException('Cette catégorie n\'existe pas');
        }
        $formEdit = $this-> createForm(AddCategoryType::class, $categorie, ['validation_groups' => 'edit_categorie']);
        $formEdit->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
           
            $categorie->setSlug((new AsciiSlugger())->slug(strtolower($categorie->getName())));

            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($categorie);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success','La catégorie a bien été modifiée !');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'formEdit' => $formEdit->createView(),
    
    ]);
    }

     /** 
     * @IsGranted("ROLE_EDITOR")
     * @Route("/admin/edit/picture/{id}", name="admin_edit_picture")
     */
    public function editPicture($id, Request $request){
        
        // Sélectionner un enregistrement en BDD
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        if (!$picture){
            throw $this->createNotFoundException('Cette image n\'existe pas');
        }
        $formEdit = $this-> createForm(UploadPictureType::class, $picture, ['validation_groups' => 'edit_picture']);
        $formEdit->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
        
            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success','Les modifications ont bien été apporté !');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin_picture');
        }

        return $this->render('admin/edit_picture.html.twig', [
            'formEdit' => $formEdit->createView(),
    
    ]);
    }

     /** 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/delete/categorie/{id}", name="admin_delete_categorie")
     */
    public function delete($id)
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($categorie);
        $doctrine->flush();

        $this->addFlash('success','La categorie à bien été supprimée');
        return $this->redirectToRoute('admin');
    }

    /** 
     * @IsGranted("ROLE_EDITOR")
     * @Route("/admin/delete/picture/{id}", name="admin_delete_picture")
     */
    public function delete_picture($id)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($picture);
        $doctrine->flush();

        $this->addFlash('success','La photo à bien été supprimée');
        return $this->redirectToRoute('admin_picture');
    }

    /** 
     * @IsGranted("ROLE_EDITOR")
     * @Route("/admin/validate/picture/{id}", name="admin_validate_picture")
     */
    public function validate_picture($id, Request $request)
    {
        $picture = $this->getDoctrine()->getRepository(Picture::class)->find($id);
      
        $formValid = $this-> createForm(ValidatePictureType::class, $picture);
        $formValid->handleRequest($request);
        
        if($formValid->isSubmitted() && $formValid->isValid()){
           
          
        $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($picture);
            $doctrine->flush();
       
        $this->addFlash('success','La photo à bien été validé');
        return $this->redirectToRoute('admin_picture');
        }
        return $this->render('admin/validate_picture.html.twig', [
            'formValid' => $formValid->createView(),
    
    ]);
    }


}
