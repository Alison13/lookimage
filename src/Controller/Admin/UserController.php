<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserController extends AbstractController
{
    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/user", name="admin_user")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {
        $query = $this->getDoctrine()->getRepository(User::class)->findAll();
    
        $page =  $request->query->getInt('page', 1);
        $users = $paginator->paginate( 
            $query, 
            $page === 0 ? 1 : $page, 
            35
        );
        return $this->render('admin/user/index.html.twig', [
            'users' => $users
        ]);
    }

    /** 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/edit/user/{id}", name="admin_edit_user")
     */
    public function edit($id, Request $request){
        
        // Sélectionner un enregistrement en BDD
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$user){
            throw $this->createNotFoundException('Cette utilisateur n\'existe pas');
        }
        $formEdit = $this-> createForm(UserType::class, $user);
        $formEdit->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if($formEdit->isSubmitted() && $formEdit->isValid()){
           
            // Insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            // Création d'un message flash
            $this->addFlash('success','L\'utilisateur a bien été modifiée !');
            // Redirection vers la page d'accueil
            return $this->redirectToRoute('admin_user');
        }

        return $this->render('admin/user/edit.html.twig', [
            'formEdit' => $formEdit->createView(),
    
    ]);
    }

    /** 
     * @IsGranted("ROLE_ADMIN")
     * @Route("/admin/delete/user/{id}", name="admin_delete_user")
     */
    public function delete($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success','L\'utilisateur à bien été supprimée');
        return $this->redirectToRoute('admin_user');
    }
}
